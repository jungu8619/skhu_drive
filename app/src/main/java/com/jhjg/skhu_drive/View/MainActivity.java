package com.jhjg.skhu_drive.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.jhjg.skhu_drive.Controller.MainActivityController;
import com.jhjg.skhu_drive.Controller.NetworkManager;
import com.jhjg.skhu_drive.Controller.UserDBManager;
import com.jhjg.skhu_drive.R;
import com.jhjg.skhu_drive.Support.Address;

import java.util.HashMap;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    public static final String TAG = "MainActivity";
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private FragmentTransaction transaction;
    private String id;
    private Menu menu;
    private Toolbar toolbar;
    private int FAVORITE_FOLDER = 0;
    private int FAVORITE_DRIVE = 1;
    private Fragment newFragment;
    private Bundle bundle;
    private ProgressDialog progressDialog;
    private HashMap<Integer,String> d_idHashMap;
    private MaterialDialog dialog;
    private FrameLayout mainFragment;
    private FloatingActionButton departmentChangeButton;
    private TextView department1,department2,department3,department4;
    private SubMenu driveSubMenu, folderSubMenu;

    private final static int DEPARTMENT_FRAGMENT = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startProgress(null);
        mainFragment = (FrameLayout) findViewById(R.id.main_fragment);
        departmentChangeButton = (FloatingActionButton) findViewById(R.id.main_department);
        departmentChangeButton.setOnClickListener(this);
        d_idHashMap = new HashMap<>();

        //MainActivityController에 context를 넣어준다
        MainActivityController.getInstance().setContext(this);

        //회원가입 유저데이터를 지워준다.
        UserDBManager.getInstance().deleteAll();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //id를 가져온다.
        id = getIntent().getStringExtra("id");

        //맨처음에 시작할때는 Department fragment를 보여준다.
        fragmentReplace(DEPARTMENT_FRAGMENT,null);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //원래 있던 헤더뷰 삭제 후
        navigationView.removeHeaderView(navigationView.getHeaderView(0));
        //헤더뷰 다시 넣어 줌
        TextView view = (TextView) navigationView.inflateHeaderView(R.layout.nav_header_main).findViewById(R.id.textView);
        view.setText(id);

        //메뉴 추가
        menu = navigationView.getMenu();
        getFavoriteList();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // fragment 영역에 fragment를 set 해주는 function
    public void fragmentReplace(int reqNewFragmentIndex,@Nullable String d_id) {
        //bundle 생성
        bundle = new Bundle();
        if (reqNewFragmentIndex == DEPARTMENT_FRAGMENT) {
            newFragment = new DepartmentFragment();  // replace fragment
            bundle.putString("d_id",d_id);
        }
        newFragment.setArguments(bundle);

        transaction = getSupportFragmentManager().beginTransaction(); // FragmentTransaction 생성
        transaction.setCustomAnimations(R.anim.layout_leftin, R.anim.layout_leftout);
        transaction.replace(R.id.main_fragment, newFragment); // fragment를 remove 후 add,
        transaction.commit();   // Commit the transaction
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //즐겨 찾기에서 눌렀을 경우
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //bundle 생성
        bundle = new Bundle();
        //즐겨찾기눌렀을경우 바로 폴더나 드라이브로 들어간다.
        if(item.getGroupId() == FAVORITE_FOLDER){
            Log.e(TAG,"FAVORITE_FOLDER");
            newFragment = new Folder_FileFragment();
            bundle.putLong("fd_id", id);
            bundle.putString("folder_name",item.getTitle().toString());
        }else if(item.getGroupId() == FAVORITE_DRIVE){
            Log.e(TAG,"FAVORITE_DRIVE");
            newFragment = new DriveFragment();
            bundle.putLong("dr_id", id);
            bundle.putString("d_id",d_idHashMap.get(id));
        }

        //번들넣기
        newFragment.setArguments(bundle);
        //transaction 생성
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //fragment animation
        transaction.setCustomAnimations(R.anim.layout_leftin, R.anim.layout_leftout, R.anim.popup_enter, R.anim.popup_exit);
        //프레그먼트교체
        transaction.replace(R.id.main_fragment, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();//이런식으로 fragment 교체가능

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    public void setToolbarTitle(String title){
        toolbar.setTitle(title);
    }

    public void getFavoriteList(){
        //즐겨찾는 드라이브 가져오기
        NetworkManager.getInstance().getList(MainActivityController.getInstance().getBaeJsonHttpResponseHandler(FAVORITE_DRIVE), Address.getInstance().getMY_FAVORITE_DRIVE_TYPE(),null,null);
        //즐겨찾는 폴더 가져오기
        NetworkManager.getInstance().getList(MainActivityController.getInstance().getBaeJsonHttpResponseHandler(FAVORITE_FOLDER), Address.getInstance().getMY_FAVORITE_FOLDER_TYPE(),null,null);
    }

    public Menu getMenu(){
        return menu;
    }

    public void addMenu(int type, int item_id, String title) {
        if (type == FAVORITE_DRIVE) {
            if(driveSubMenu == null) {
                driveSubMenu = menu.addSubMenu("즐겨찾는 드라이브");
            }
            driveSubMenu.add(type,item_id,0,title);
        }else if(type == FAVORITE_FOLDER){
            if(folderSubMenu == null) {
                folderSubMenu = menu.addSubMenu("즐겨찾는 폴더");
            }
            folderSubMenu.add(type,item_id,0,title);
        }

    }

    //startProgress 시작
    public void startProgress(@Nullable String message) {
        if(progressDialog == null) {
            if(message == null)
                message = "Loading...";
            progressDialog = progressDialog.show(this,message , "잠시만 기다려 주세요.");
        }
    }

    //stopProgress 끝내기
    public void stopProgress() {
        if(progressDialog !=null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void setDialog(String title, String positiveText, final String[] parameterName, final String[] parameter, final int type,@Nullable final String name) {
        dialog = new MaterialDialog.Builder(this)
                .title(title)
                .negativeText("취소")
                .positiveText(positiveText)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        NetworkManager.getInstance().post(mainFragment, type, parameterName, parameter, name);
                    }
                }).build();

        dialog.show();
    }

    public void setD_idHashMap(int key, String value){
        d_idHashMap.put(key,value);
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == departmentChangeButton.getId()){
            Log.e(TAG,"department 눌림");

            //학과 선택 눌렀을시에
            dialog = new MaterialDialog.Builder(this)
                    .title("학과선택")
                    .customView(R.layout.department_dialog, true)
                    .negativeText("취소")
                    .negativeColor(Color.parseColor("#bd5151"))
                    .build();

            department1 = (TextView) dialog.getCustomView().findViewById(R.id.department1);
            department2 = (TextView) dialog.getCustomView().findViewById(R.id.department2);
            department3 = (TextView) dialog.getCustomView().findViewById(R.id.department3);
            department4 = (TextView) dialog.getCustomView().findViewById(R.id.department4);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == department1.getId()){
                        fragmentReplace(DEPARTMENT_FRAGMENT,"1");
                        dialog.dismiss();
                    }else if(v.getId() == department2.getId()){
                        fragmentReplace(DEPARTMENT_FRAGMENT,"2");
                        dialog.dismiss();
                    }else if(v.getId() == department3.getId()){
                        fragmentReplace(DEPARTMENT_FRAGMENT,"3");
                        dialog.dismiss();
                    }else if(v.getId() == department4.getId()){
                        fragmentReplace(DEPARTMENT_FRAGMENT,"4");
                        dialog.dismiss();
                    }
                }
            };
            department1.setOnClickListener(listener);
            department2.setOnClickListener(listener);
            department3.setOnClickListener(listener);
            department4.setOnClickListener(listener);

            dialog.show();


        }
    }
}

