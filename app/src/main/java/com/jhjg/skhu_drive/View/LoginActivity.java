package com.jhjg.skhu_drive.View;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.jhjg.skhu_drive.Controller.JoinHelper;
import com.jhjg.skhu_drive.Controller.LoginHelper;
import com.jhjg.skhu_drive.Controller.SettingManager;
import com.jhjg.skhu_drive.GCM.QuickstartPreferences;
import com.jhjg.skhu_drive.GCM.RegistrationIntentService;
import com.jhjg.skhu_drive.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity {

    public static final String TAG = "LoginActivity";
    private com.rengwuxian.materialedittext.MaterialEditText loginid;
    private com.rengwuxian.materialedittext.MaterialEditText loginpassword;
    private com.dd.processbutton.iml.ActionProcessButton loginaction;
    private android.support.v7.widget.AppCompatTextView loginjoin;
    private String[] login;
    private boolean sentToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //퍼미션체크
        checkPermission();
        //gcm을 받아올 broadcast 등록
        registBroadcastReceiver();
        //key 얻어오기
        getInstanceIdToken();

        this.loginjoin = (AppCompatTextView) findViewById(R.id.login_join);
        this.loginaction = (ActionProcessButton) findViewById(R.id.login_action);
        this.loginpassword = (MaterialEditText) findViewById(R.id.login_password);
        this.loginid = (MaterialEditText) findViewById(R.id.login_id);

        login = new String[2];

        //로그인 버튼 눌렀을때
        loginaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login[0] = loginid.getText().toString();
                login[1] = loginpassword.getText().toString();

                LoginHelper.getInstance().sendLogin(LoginActivity.this, login);

            }
        });

        loginjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JoinHelper.getInstance().sendJoin(LoginActivity.this);
            }
        });

    }

    /**
     * Permission check.
     */

    //퍼미션 리스너
    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            Toast.makeText(LoginActivity.this, "구글드라이브 연동 및 파일 다운로드 권한 허용", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> arrayList) {
            Toast.makeText(LoginActivity.this, "권한 거부", Toast.LENGTH_SHORT).show();
        }
    };


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            new TedPermission(this)
                    .setPermissionListener(permissionListener)
                    .setRationaleMessage("파일 다운로드와 업로드를 위해 읽기/쓰기 권한이 필요합니다.")
                    .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
                    .check();

            new TedPermission(this)
                    .setPermissionListener(permissionListener)
                    .setRationaleMessage("구글드라이브와 연동을 사용하기 위해 권한이 필요합니다.")
                    .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                    .setPermissions(Manifest.permission.GET_ACCOUNTS)
                    .check();

        }
    }



    //GCM 관련 메소드

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    /**
     * Instance ID를 이용하여 디바이스 토큰을 가져오는 RegistrationIntentService를 실행한다.
     */
    public void getInstanceIdToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * LocalBroadcast 리시버를 정의한다. 토큰을 획득하기 위한 READY, GENERATING, COMPLETE 액션에 따라 UI에 변화를 준다.
     */
    public void registBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG,"receive 받음");
                sentToken = SettingManager.getInstance().getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER);

                if(sentToken){
                    Log.e(TAG,"성공");
                }else {
                    Log.e(TAG,"실패");
                }

            }
        };

    }

    /**
     * 앱이 실행되어 화면에 나타날때 LocalBoardcastManager에 액션을 정의하여 등록한다.
     */
    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_READY));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_GENERATING));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));

    }

    /**
     * 앱이 화면에서 사라지면 등록된 LocalBoardcast를 모두 삭제한다.
     */
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        LoginHelper.getInstance().dismissProgress();
        super.onPause();
    }


    //사용할수있는환경인지 체크.
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}

