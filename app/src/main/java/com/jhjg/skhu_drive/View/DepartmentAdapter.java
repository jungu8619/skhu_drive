package com.jhjg.skhu_drive.View;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jhjg.skhu_drive.Controller.SettingManager;
import com.jhjg.skhu_drive.Model.DriveData;
import com.jhjg.skhu_drive.R;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;
import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.List;

/**
 * Created by kangjungu1 on 2016. 4. 7..
 */
public class DepartmentAdapter extends RecyclerView.Adapter<DepartmentAdapter.ViewHolder> {

    public static final String TAG = "DepartmentAdapter";
    public List<DriveData> items;
    private DriveData item;
    private Context context;
    //false이면 리스트, true이면 그리드

    public DepartmentAdapter(Context context, List<DriveData> items) {
        Log.e(TAG,"adapter 생성");
        this.items = items;
        this.context = context;
    }

    //뷰홀더를 그려서 리턴
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        //gridView
        if(SettingManager.getInstance().getBoolean(SettingManager.getActionView())){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.professor_grid_list_item, parent, false);
            view.findViewById(R.id.professor_name).setVisibility(View.GONE);
        }else {
        //listView
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.professor_list_item, parent, false);
        }
        return new ViewHolder(view);
    }

    //데이터와 뷰를 묶어줌
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        item = items.get(position);

        holder.name.setText(item.getP_name());
        holder.content.setText(item.getDrive_name());
        holder.icon.setIcon(MaterialDrawableBuilder.IconValue.CLOUD_OUTLINE);

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListenr != null){
                    item = items.get(position);
                    mListenr.onItemClicked(item.getDrive_id(),item.getDrive_name());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getDrive_id();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, content;
        private LinearLayout item;
        private MaterialIconView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.professor_name);
            content = (TextView) itemView.findViewById(R.id.professor_content);
            item = (LinearLayout) itemView.findViewById(R.id.professor_item);
            icon = (MaterialIconView)itemView.findViewById(R.id.professor_icon);
        }
    }

    public interface ProfessorItemClickListener {
        void onItemClicked(long dr_id,String drive_name);
    }

    private ProfessorItemClickListener mListenr;

    public void setProfessorItemClickListener(ProfessorItemClickListener listener){
        mListenr = listener;
    }
}
