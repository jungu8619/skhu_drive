package com.jhjg.skhu_drive.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jhjg.skhu_drive.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    //font 설정
//
//    @Override
//    protected void attachBaseContext(Context newBase) {
//
//        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
//
//    }
}
