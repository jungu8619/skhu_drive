package com.jhjg.skhu_drive.View;

/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataChangeSet;
import com.jhjg.skhu_drive.Controller.FileManager;
import com.jhjg.skhu_drive.Support.MyApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

public class GoogleDriveActivity extends Activity implements ConnectionCallbacks,
        OnConnectionFailedListener {

    private static final String TAG = "drive-quickstart";
    private static final int REQUEST_CODE_RESOLUTION = 3;
    private final int BUF_SIZE = 512;
    private final String PATH_KEY = "path";
    private GoogleApiClient mGoogleApiClient;
    private String path;


    @Override
    protected void onResume() {
        super.onResume();
        path = getIntent().getStringExtra(PATH_KEY);
        if (mGoogleApiClient == null) {
            // api Client build함
            // account name이 안돌아오면 다시 선택 시도한다.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        //client에 connect
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        //api client 연결 실패시
        Log.e(TAG, "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            //에러다이얼로그띄워준다.
            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
            return;
        }
        // 아직 인증이 안된상태임. 인증 다이얼로그를 띄워준다.
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (SendIntentException e) {
            Log.e(TAG, "Exception while starting resolution activity", e);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.e(TAG, "API client connected.");
        if(path != null){
            File f = new File(path);
            saveFileToDrive(f);
        }
        if(path == null) {
            //api client 연결된경우
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.e(TAG, "GoogleApiClient connection suspended");
        //googleAPi connection이 유예된경우
    }

    /**
     * Create a new file and save it to Drive.
     */
    public void saveFileToDrive(final File f) {
        // 구글드라이브에 파일올리기
        Log.e(TAG, "Creating new contents.");
        Drive.DriveApi.newDriveContents(mGoogleApiClient)
                .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {

                    @Override
                    public void onResult(DriveApi.DriveContentsResult result) {
                        //작동실패시 아무것도안하고 끝냄
                        if (!result.getStatus().isSuccess()) {
                            Log.e(TAG, "Failed to create new contents.");
                            return;
                        }
                        //작동성공시 새로운파일을올린다.
                        Log.e(TAG, "New contents created.");
                        // contents를 위한 output Stream을 얻는다.
                        OutputStream outputStream = result.getDriveContents().getOutputStream();
                        // Write the bitmap data from it.
                        // 클래스 내부공간에 바이트 배열을저장하는 클래스
                        ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();

                        //file 내용을 저장해서 ByteArrayOutputStream에 쏴준다.
                        FileInputStream fin = null;
                        try {
                            fin = new FileInputStream(f);
                            int readCount = 0;
                            byte[] inBuf = new byte[BUF_SIZE];
                            //-1이 나올때까지 계속 반복문을돌아 write
                            while ((readCount = fin.read(inBuf)) != -1) {
                                bitmapStream.write(inBuf, 0, readCount);
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.e(TAG, "file size " + bitmapStream.size());

                        try {
                            outputStream.write(bitmapStream.toByteArray());
                        } catch (IOException e1) {
                            Log.e(TAG, "Unable to write file contents.");
                        }
                        // 파일타입과 이름을 지정해준다. 사용자가 나중에 직접 바꿀수 있음.
                        MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                                .setMimeType(FileManager.getInstance().getMIMEType(f)).setTitle(f.getName()).build();
                        // 파일 선택을 위한 intent를만들고 실행.
                        IntentSender intentSender = Drive.DriveApi
                                .newCreateFileActivityBuilder()
                                .setInitialMetadata(metadataChangeSet)
                                .setInitialDriveContents(result.getDriveContents())
                                .build(mGoogleApiClient);
                        try {
                            MyApplication.getContet().startIntentSender(intentSender, null, 0, 0, 0);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Failed to launch file chooser.");
                        }finally {
                            Log.e(TAG, "finish");
                            setResult(RESULT_OK);
                            finish();
                        }


                    }
                });
    }
}