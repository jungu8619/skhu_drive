package com.jhjg.skhu_drive.View;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.jhjg.skhu_drive.Controller.SettingManager;
import com.jhjg.skhu_drive.R;
import com.jhjg.skhu_drive.Support.MyApplication;

import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.ArrayList;

public class SettingActivity extends BaseActivity implements View.OnClickListener {

    private android.widget.CheckBox googleCheck;
    private Intent intent;
    private boolean checkBoolean;
    private static final int REQUEST_SUCCESS = 1;
    private final String GOOGLE_CHECKED_KEY = "googleCheck";
    private final String PREF_ACCOUNT_NAME = "accountName";
    public static final String TAG = "SettingActivity";
    private android.support.v7.widget.Toolbar toolbar;
    private net.steamcrafted.materialiconlib.MaterialIconView settingcopyright;
    private android.widget.LinearLayout settingdownload;
    private android.widget.LinearLayout settingpush;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        this.settingpush = (LinearLayout) findViewById(R.id.setting_push);
        this.settingdownload = (LinearLayout) findViewById(R.id.setting_download);
        this.settingcopyright = (MaterialIconView) findViewById(R.id.setting_copyright);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.googleCheck = (CheckBox) findViewById(R.id.setting_checkBox);
        setPermission();

        toolbar.setTitle("Settings");

        //setting 값 가져오기
        checkBoolean = SettingManager.getInstance().getBoolean(GOOGLE_CHECKED_KEY);
        //체크 값 설정
        googleCheck.setChecked(checkBoolean);

        //클릭리스너
        googleCheck.setOnClickListener(this);
        settingcopyright.setOnClickListener(this);
        settingdownload.setOnClickListener(this);
        settingpush.setOnClickListener(this);

    }

    public void setPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck1 = ContextCompat.checkSelfPermission(MyApplication.getContet(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCheck2 = ContextCompat.checkSelfPermission(MyApplication.getContet(), Manifest.permission.READ_EXTERNAL_STORAGE);
            int permissionCheck3 = ContextCompat.checkSelfPermission(MyApplication.getContet(), Manifest.permission.GET_ACCOUNTS);

            if (permissionCheck1 == PackageManager.PERMISSION_DENIED && permissionCheck2 == PackageManager.PERMISSION_DENIED) {
                //다운로드 권한 없음
            } else {
                //다운로드권한 있음
            }


            if (permissionCheck3 == PackageManager.PERMISSION_DENIED) {
                //account 권한없음
            } else {
                //account 권한있음
            }

        } else {
            //마시멜로우 이하버전은 보여주지 않는다.
            settingdownload.setVisibility(View.GONE);
            settingpush.setVisibility(View.GONE);
        }
    }

    //퍼미션 리스너
    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            Toast.makeText(SettingActivity.this, "권한 허용.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> arrayList) {
            Toast.makeText(SettingActivity.this, "권한 거부" , Toast.LENGTH_SHORT).show();
        }
    };


    private void checkDownloadPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            new TedPermission(this)
                    .setPermissionListener(permissionListener)
                    .setRationaleMessage("파일 다운로드와 업로드를 위해 읽기/쓰기 권한이 필요합니다.")
                    .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                    .check();
        }
    }

    private void checkAccountPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            new TedPermission(this)
                    .setPermissionListener(permissionListener)
                    .setRationaleMessage("구글드라이브와 연동을 사용하기 위해 권한이 필요합니다.")
                    .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                    .setPermissions(Manifest.permission.GET_ACCOUNTS)
                    .check();
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.setting_checkBox) {
            if (!googleCheck.isChecked()) {
                //체크 되어 있을경우.
                Log.e(TAG, "isChecked " + googleCheck.isChecked());
                SettingManager.getInstance().setBoolean(GOOGLE_CHECKED_KEY, false);
                SettingManager.getInstance().remove(PREF_ACCOUNT_NAME);
            } else {
                //체크 되어 있지 않을 경우.
                Log.e(TAG, "not checked " + googleCheck.isChecked());
                intent = new Intent(this, GoogleDriveActivity.class);
                startActivityForResult(intent, REQUEST_SUCCESS);
            }
        } else if (v.getId() == settingcopyright.getId()) {
            //저작권 정보 액티비티를 띄워준다.
            intent = new Intent(this, CopyRightActivity.class);
            startActivity(intent);
        } else if (v.getId() == settingdownload.getId()) {
            //다운로드 체크 눌린경우
            checkDownloadPermission();
        } else if (v.getId() == settingpush.getId()) {
            //푸쉬 체크 눌린경우
            checkAccountPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "request " + requestCode);
        Log.e(TAG, "resultCode " + resultCode);

        switch (requestCode) {
            case REQUEST_SUCCESS:
                //성공적으로 연결되었을 시
                //check값 저장
                if (resultCode == RESULT_OK) {
                    Snackbar.make((this.findViewById(R.id.setting_activity)), "연결 완료", Snackbar.LENGTH_LONG).show();
                    SettingManager.getInstance().setBoolean(GOOGLE_CHECKED_KEY, true);
                } else {
                    googleCheck.setChecked(false);
                    Snackbar.make((this.findViewById(R.id.setting_activity)), "연결 실패", Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }
}
