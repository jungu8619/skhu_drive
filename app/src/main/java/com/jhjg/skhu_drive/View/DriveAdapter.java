package com.jhjg.skhu_drive.View;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jhjg.skhu_drive.Controller.SettingManager;
import com.jhjg.skhu_drive.Model.FolderData;
import com.jhjg.skhu_drive.Model.ParentFile;
import com.jhjg.skhu_drive.R;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;
import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.List;

/**
 * Created by kangjungu1 on 2016. 4. 10..
 */
public class DriveAdapter extends RecyclerView.Adapter<DriveAdapter.ViewHolder>{
    public static final String TAG = "DriveAdapter";
    public List<ParentFile> items;
    private FolderData folderData;
    private Context context;
    private int SFOLDER_TYPE = 0;
    private int FOLDER_TYPE = 1;

    public DriveAdapter(Context context, List<ParentFile> items) {
        this.items = items;
        this.context = context;
    }

    //뷰홀더를 그려서 리턴
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        //gridView
        if(SettingManager.getInstance().getBoolean(SettingManager.getActionView())){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.professor_grid_list_item, parent, false);
            view.findViewById(R.id.professor_name).setVisibility(View.GONE);
        }else {
            //listView
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.professor_list_item, parent, false);
        }

        return new ViewHolder(view);
    }

    //데이터와 뷰를 묶어줌
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        folderData = (FolderData)items.get(position);

        if(folderData.getFolder_name() == "null"){
            //sFolder일때
            holder.name.setText(folderData.getSfolder_name());
            holder.icon.setIcon(MaterialDrawableBuilder.IconValue.FOLDER_LOCK_OPEN);
        }else {
            holder.name.setText(folderData.getFolder_name());
        }
        holder.folderSize.setText("");
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    folderData = (FolderData)items.get(position);
                    Log.e(TAG,"click "+folderData.getFolder_id());

                    if(folderData.getFolder_name() == "null"){
                        //sFolder일때
                        Log.e(TAG,"sfolder 클릭 "+folderData.getSfolder_name());
                        mListener.onItemClicked(folderData.getFolder_id(),folderData.getSfolder_name(),SFOLDER_TYPE);
                    }else {
                        mListener.onItemClicked(folderData.getFolder_id(), folderData.getFolder_name(), FOLDER_TYPE);
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        folderData = (FolderData) items.get(position);
        return folderData.getParent_id();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, folderSize;
        private LinearLayout item;
        private MaterialIconView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            folderSize = (TextView) itemView.findViewById(R.id.professor_name);
            name = (TextView) itemView.findViewById(R.id.professor_content);
            item = (LinearLayout) itemView.findViewById(R.id.professor_item);
            icon = (MaterialIconView)itemView.findViewById(R.id.professor_icon);
        }
    }


    public interface FolderItemClickListener {
        void onItemClicked(long folder_id,String folder_name,int type);
    }

    private FolderItemClickListener mListener;

    public void setFolderItemClickListener(FolderItemClickListener listener){
        mListener = listener;
    }

}
