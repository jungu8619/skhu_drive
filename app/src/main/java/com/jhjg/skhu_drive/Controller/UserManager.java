package com.jhjg.skhu_drive.Controller;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

/**
 * Created by kangjungu1 on 2016. 6. 22..
 */
public class UserManager {
    private static UserManager ourInstance = new UserManager();

    public static UserManager getInstance() {
        return ourInstance;
    }

    private UserManager() {
    }

    /**
     * @param extension : 확장자를받아서
     * @return logo 이미지를 넘겨준다.
     */
    public MaterialDrawableBuilder.IconValue getImageResource(String extension){
        if(extension.contains("ppt")) {
            return MaterialDrawableBuilder.IconValue.FILE_POWERPOINT;
        }else if(extension.contains("zip")){
            return MaterialDrawableBuilder.IconValue.ZIP_BOX;
        }else if(extension.contains("jpg") || extension.contains("jpeg") || extension.contains("jpe") || extension.contains("png")){
            return MaterialDrawableBuilder.IconValue.FILE_IMAGE;
        }else if(extension.contains("xls")){
            return MaterialDrawableBuilder.IconValue.FILE_EXCEL;
        }else if(extension.contains("doc")){
            return MaterialDrawableBuilder.IconValue.FILE_DOCUMENT;
        }else if(extension.contains("js")){
            return MaterialDrawableBuilder.IconValue.LANGUAGE_JAVASCRIPT;
        }else if(extension.contains("java") || extension.equals("c") || extension.equals("h")){
            return MaterialDrawableBuilder.IconValue.CODE_NOT_EQUAL_VARIANT;
        }else{
            return MaterialDrawableBuilder.IconValue.FILE;
        }
    }

}
