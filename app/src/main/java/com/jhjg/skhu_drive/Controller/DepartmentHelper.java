package com.jhjg.skhu_drive.Controller;

/**
 * Created by kangjungu1 on 2016. 6. 25..
 */
public class DepartmentHelper {
    private static DepartmentHelper ourInstance = new DepartmentHelper();

    public static DepartmentHelper getInstance() {
        return ourInstance;
    }

    private DepartmentHelper() {
    }

    public String getDepartmentName(String d_id){
        if(d_id == null){
            return "SKHU_Drive";
        }

        if(d_id.equals("1")){
            return "소프트웨어공학과";
        }else if(d_id.equals("2")){
            return "컴퓨터공학과";
        }else if(d_id.equals("3")){
            return "정보통신공학과";
        }else if(d_id.equals("4")){
            return "글로컬IT학과";
        }

        return "SKHU_Drive";
    }
}
