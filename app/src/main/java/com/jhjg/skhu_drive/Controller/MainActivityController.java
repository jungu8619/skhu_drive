package com.jhjg.skhu_drive.Controller;

import android.content.Context;
import android.util.Log;

import com.jhjg.skhu_drive.View.MainActivity;
import com.loopj.android.http.BaseJsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by kangjungu1 on 2016. 6. 24..
 */
public class MainActivityController {
    private static Context context;
    private static MainActivityController ourInstance = new MainActivityController();

    public static MainActivityController getInstance() {
        return ourInstance;
    }

    private MainActivityController() {
    }
    public static final String TAG = "MainActivityController";
    private int FAVORITE_FOLDER = 0;
    private int FAVORITE_DRIVE = 1;

    public void setContext(Context context){
        this.context = context;
    }

    public BaseJsonHttpResponseHandler<JSONArray> getBaeJsonHttpResponseHandler(final int type){

        return new BaseJsonHttpResponseHandler<JSONArray>() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, JSONArray response) {
                JSONObject jsonObj;
                Log.e(TAG, "onSuccess "+ response.length());


                try {
                    for (int i = 0; i < response.length(); i++) {
                        jsonObj = response.getJSONObject(i);
                        if(type == FAVORITE_FOLDER){
                            //json을 가져와 item에 넣어준다.
                            ((MainActivity)context).addMenu(type,jsonObj.getInt("folder_id"),jsonObj.getString("folder_name"));
                        }else if(type == FAVORITE_DRIVE){
                            ((MainActivity)context).addMenu(type,jsonObj.getInt("drive_id"),jsonObj.getString("drive_name"));
                            ((MainActivity)context).setD_idHashMap(jsonObj.getInt("drive_id"),jsonObj.getString("d_id"));
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "error" + e.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, JSONArray errorResponse) {
                Log.e(TAG, "onFailure" + throwable.getMessage());
            }

            @Override
            protected JSONArray parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                if (!isFailure) {
                    Log.e(TAG, "parseResponse" + rawJsonData);
                    return new JSONArray(rawJsonData);
                }
                return null;
            }
        };
    }


}
