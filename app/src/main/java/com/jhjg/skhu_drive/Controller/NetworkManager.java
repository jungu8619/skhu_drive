package com.jhjg.skhu_drive.Controller;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.jhjg.skhu_drive.Support.Address;
import com.jhjg.skhu_drive.Support.MyApplication;
import com.jhjg.skhu_drive.View.DepartmentFragment;
import com.jhjg.skhu_drive.View.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

/**
 * Created by kangjungu1 on 2016. 5. 16..
 */
public class NetworkManager {


    private static NetworkManager ourInstance = new NetworkManager();

    public static NetworkManager getInstance() {
        return ourInstance;
    }

    public static final String TAG = "NetworkManager";


    private PersistentCookieStore myCookieStore;
    private Cookie storeCookie;
    private AsyncHttpClient client;
    private String url;
    private String title;
    private Context mContext;
    private int CURRENT_USER_TYPE = 0;
    private int FAVORITE_FOLDER = 0;
    private int FAVORITE_DRIVE = 1;
    private int SFOLDER_TYPE = 0;

    private NetworkManager() {
        myCookieStore = new PersistentCookieStore(MyApplication.getContet());
        storeCookie = myCookieStore.getCookies().get(0);
        client = new AsyncHttpClient();
        client.addHeader("Cookie", "JSESSIONID=" + storeCookie.getValue().substring(11, storeCookie.getValue().indexOf("; Path=/")));
    }

    // 리스트 받아올때 사용
    public void getList(BaseJsonHttpResponseHandler<JSONArray> handler, int type, @Nullable String[] parameterName, @Nullable String[] parameter) {
        RequestParams params = new RequestParams();

        if (parameterName != null && parameter != null) {
            for (int i = 0; i < parameterName.length; i++) {
                params.add(parameterName[i], parameter[i]);
            }
        }

        url = Address.getInstance().getURL(type);
        client.get(MyApplication.getContet(), url, params, handler);

    }

    //바이너리 데이터 -> 다운로드 받을때 사용
    public void getList(BinaryHttpResponseHandler handler, int type, String[] parameterName, String[] parameter) {
        RequestParams params = new RequestParams();

        for (int i = 0; i < parameterName.length; i++) {
            params.add(parameterName[i], parameter[i]);
        }

        url = Address.getInstance().getURL(type);
        client.get(MyApplication.getContet(), url, params, handler);

    }

    public void setRegistrationID(int type, String[] parameterName, String[] parameter) {
        RequestParams params = new RequestParams();
        for (int i = 0; i < parameterName.length; i++) {
            params.add(parameterName[i], parameter[i]);
        }
        url = Address.getInstance().getURL(type);
        Log.e(TAG, "url " + url);
        client.get(MyApplication.getContet(), url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.e(TAG, "success");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(TAG, "failure" + error);
                Log.e(TAG, "failure" + error.getLocalizedMessage());
            }
        });
    }

    public void getD_id(final Fragment f, int type) {


        url = Address.getInstance().getURL(type);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(TAG, "responseString" + responseString);
                Log.e(TAG, "throwable" + throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e(TAG, "responseString " + responseString);
                ((DepartmentFragment) f).setD_id(responseString);
                //Data를 가져온다.
                ((DepartmentFragment) f).getData();


            }
        });
    }

    //post
    public void post(@Nullable final View v, final int type, @Nullable final String[] parameterName, @Nullable final String[] parameter, @Nullable final String name) {
        final RequestParams params = new RequestParams();

        if (parameterName != null && parameter != null) {
            for (int i = 0; i < parameterName.length; i++) {
                params.add(parameterName[i], parameter[i]);
            }
        }

        url = Address.getInstance().getURL(type);
        Log.e(TAG,"url "+url);
        client.post(MyApplication.getContet(), url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                //드라이브 즐겨찾기추가일경우
                if(type == Address.getInstance().getADD_FAVORITE_DRIVE_TYPE()) {
                    title = "드라이브가 즐겨찾기에 추가되었습니다.";
                    ((MainActivity)v.getContext()).addMenu(FAVORITE_DRIVE,Integer.parseInt(parameter[1]),name);
                    ((MainActivity)v.getContext()).setD_idHashMap(Integer.parseInt(parameter[1]),parameter[0]);
                }
                //폴더 즐겨찾기추가일경우
                if(type == Address.getInstance().getADD_FAVORITE_SUBFOLDER_TYPE()){
                    title = "폴더가 즐겨찾기에 추가되었습니다.";
                    ((MainActivity)v.getContext()).addMenu(FAVORITE_FOLDER,Integer.parseInt(parameter[1]),name);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if(v != null) {
                    title = "성공하였습니다.";

                    //드라이브 즐겨찾기추가일경우
                    if(type == Address.getInstance().getADD_FAVORITE_DRIVE_TYPE()) {
                        title = "드라이브가 즐겨찾기에 추가되었습니다.";
                        ((MainActivity)v.getContext()).addMenu(FAVORITE_DRIVE,Integer.parseInt(parameter[1]),name);
                        ((MainActivity)v.getContext()).setD_idHashMap(Integer.parseInt(parameter[1]),parameter[0]);
                    }
                    //폴더 즐겨찾기추가일경우
                    if(type == Address.getInstance().getADD_FAVORITE_SUBFOLDER_TYPE()){
                        title = "폴더가 즐겨찾기에 추가되었습니다.";
                        ((MainActivity)v.getContext()).addMenu(FAVORITE_FOLDER,Integer.parseInt(parameter[1]),name);
                    }

                    Snackbar.make(v,title , Snackbar.LENGTH_LONG).show();
                }
            }
        });


    }

    public void get(AsyncHttpResponseHandler handler, int type, @Nullable String[] parameterName, @Nullable String[] parameter){

        RequestParams params = new RequestParams();

        if (parameterName != null && parameter != null) {
            for (int i = 0; i < parameterName.length; i++) {
                params.add(parameterName[i], parameter[i]);
            }
        }

        url = Address.getInstance().getURL(type);
        client.get(MyApplication.getContet(), url, params, handler);

    }

    public TextHttpResponseHandler getTextHandler(final int type){
        return new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if(type == SFOLDER_TYPE)
                    Log.e(TAG,"getTextHandler fail"+throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if(type == SFOLDER_TYPE)
                    Log.e(TAG,"getTextHandler success "+responseString);
            }
        };
    }

}
