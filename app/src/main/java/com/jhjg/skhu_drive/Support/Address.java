package com.jhjg.skhu_drive.Support;

/**
 * Created by kangjungu1 on 2016. 5. 16..
 */
public class Address {
    private final String DOMAIN = "172.20.10.11";
    private final String BASE_URL = "http://" + DOMAIN + ":8080";
    private final String MAIN_PROFESSOR_DRIVE_URL = "/drive/pdrive/mainJSON.pd";
    private final String FOLDER_LIST1_URL = "/drive/pdrive/folderListJSON.pd";
    private final String FOLDER_LIST2_URL = "/drive/pdrive/folderList2JSON.pd";
    private final String FILE_LIST_URL = "/drive/pdrive/folderList2JSON2.pd";
    private final String JOIN_URL = "/drive/home/user_join.pd";
    private final String LOGIN_URL = "/drive/home/login_processing.pd";
    private final String DOWNLOAD_URL = "/drive/pdrive/downloadJSON.pd";
    private final String MY_FAVORITE_FOLDER_URL = "/drive/user/mypageJSON.pd";
    private final String MY_FAVORITE_DRIVE_URL = "/drive/user/mypageJSON2.pd";
    private final String SET_REGISTRATION_URL = "/drive/user/Registration.pd";
    private final String GET_D_ID_URL = "/drive/user/d_id.pd";
    private final String ADD_FAVORITE_SUBFOLDER_URL = "/drive/pdrive/folderList2.pd";
    private final String ADD_FAVORITE_DRIVE_URL = "/drive/pdrive/main.pd";
    private final String SFOLDER_URL = "/drive/pdrive/sfolderList2JSON.pd";
    private final String SFOLDER_CHECK_URL = "/drive/pdrive/sfolderCheck.pd";

    //type
    private final int PROFESSOR_TYPE = 0;
    private final int MAIN_FOLDER_TYPE = 1;
    private final int FOLDER_FILE_TYPE = 2;
    private final int FILE_TYPE = 3;
    private final int DOWNLOAD_TYPE = 4;
    private final int MY_FAVORITE_FOLDER_TYPE = 5;
    private final int SET_REGISTRATION_TYPE = 6;
    private final int MY_FAVORITE_DRIVE_TYPE = 7;
    private final int GET_D_ID_TYPE = 8;
    private final int ADD_FAVORITE_SUBFOLDER_TYPE = 10;
    private final int ADD_FAVORITE_DRIVE_TYPE = 11;
    private final int SFOLDER_TYPE = 12;
    private final int SFOLDER_CHECK_TYPE =13;

    private String url;

    private static Address ourInstance = new Address();

    public static Address getInstance() {
        return ourInstance;
    }

    private Address() {
    }

    //URL RETURN
    //맨처음 페이지
    private String getProfessorURL() {
        return BASE_URL + MAIN_PROFESSOR_DRIVE_URL;
    }

    //처음 폴더 눌렀을경우
    private String getMainFolderURL() {
        return BASE_URL + FOLDER_LIST1_URL;
    }

    //폴더 눌러서 안에 들어온 경우
    private String getFolderFileURL() {
        return BASE_URL + FOLDER_LIST2_URL;
    }

    public String getJoinURL() {
        return BASE_URL + JOIN_URL;
    }

    public String getLoginURL() {
        return BASE_URL + LOGIN_URL;
    }

    private String getFileURL() {
        return BASE_URL + FILE_LIST_URL;
    }

    private String getDownloadUrl() {
        return BASE_URL + DOWNLOAD_URL;
    }

    private String getMyFavoriteFolderUrl() {
        return BASE_URL + MY_FAVORITE_FOLDER_URL;
    }

    private String getSetRegistrationUrl() {
        return BASE_URL + SET_REGISTRATION_URL;
    }

    private String getMY_FAVORITE_Drive_URL() {
        return BASE_URL + MY_FAVORITE_DRIVE_URL;
    }

    private String getGetDIdUrl() {
        return BASE_URL + GET_D_ID_URL;
    }


    private String getADD_FAVORITE_SUBFOLDER_URL() {
        return BASE_URL+ADD_FAVORITE_SUBFOLDER_URL;
    }

    private String getADD_FAVORITE_DRIVE_URL() {
        return BASE_URL+ADD_FAVORITE_DRIVE_URL;
    }

    private String getSFOLDER_URL() {
        return BASE_URL+SFOLDER_URL;
    }

    private String getSFOLDER_CHECK_URL() {
        return BASE_URL+SFOLDER_CHECK_URL;
    }

    public String getDomain() {
        return DOMAIN;
    }

    //TYPE RETURN
    public int getFolderFileType() {
        return FOLDER_FILE_TYPE;
    }

    public int getProfessorType() {
        return PROFESSOR_TYPE;
    }

    public int getMainFolderType() {
        return MAIN_FOLDER_TYPE;
    }

    public int getFILE_TYPE() {
        return FILE_TYPE;
    }

    public int getDOWNLOAD_TYPE() {
        return DOWNLOAD_TYPE;
    }

    public int getMY_FAVORITE_FOLDER_TYPE() {
        return MY_FAVORITE_FOLDER_TYPE;
    }

    public int getSET_REGISTRATION_TYPE() {
        return SET_REGISTRATION_TYPE;
    }

    public int getMY_FAVORITE_DRIVE_TYPE() {
        return MY_FAVORITE_DRIVE_TYPE;
    }

    public int getGET_D_ID_TYPE() {
        return GET_D_ID_TYPE;
    }


    public int getADD_FAVORITE_SUBFOLDER_TYPE() {
        return ADD_FAVORITE_SUBFOLDER_TYPE;
    }

    public int getADD_FAVORITE_DRIVE_TYPE() {
        return ADD_FAVORITE_DRIVE_TYPE;
    }

    public int getSFOLDER_TYPE() {
        return SFOLDER_TYPE;
    }

    public int getSFOLDER_CHECK_TYPE() {
        return SFOLDER_CHECK_TYPE;
    }


    //타입에따라 URL 리턴
    public String getURL(int type) {
        if (type == PROFESSOR_TYPE) {
            url = getProfessorURL();
        } else if (type == MAIN_FOLDER_TYPE) {
            url = getMainFolderURL();
        } else if (type == FOLDER_FILE_TYPE) {
            url = getFolderFileURL();
        } else if (type == FILE_TYPE) {
            url = getFileURL();
        } else if (type == DOWNLOAD_TYPE) {
            url = getDownloadUrl();
        } else if (type == MY_FAVORITE_FOLDER_TYPE) {
            url = getMyFavoriteFolderUrl();
        } else if (type == SET_REGISTRATION_TYPE) {
            url = getSetRegistrationUrl();
        } else if (type == MY_FAVORITE_DRIVE_TYPE) {
            url = getMY_FAVORITE_Drive_URL();
        } else if (type == GET_D_ID_TYPE) {
            url = getGetDIdUrl();
        } else if (type == ADD_FAVORITE_DRIVE_TYPE){
            url = getADD_FAVORITE_DRIVE_URL();
        } else if (type == ADD_FAVORITE_SUBFOLDER_TYPE){
            url = getADD_FAVORITE_SUBFOLDER_URL();
        } else if (type == SFOLDER_TYPE){
            url = getSFOLDER_URL();
        }else if(type == SFOLDER_CHECK_TYPE){
            url = getSFOLDER_CHECK_URL();
        }

        return url;

    }


}
