package com.jhjg.skhu_drive.GCM;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.jhjg.skhu_drive.Controller.SettingManager;
import com.jhjg.skhu_drive.R;
import com.jhjg.skhu_drive.Support.MyApplication;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {


    private  static RegistrationIntentService ourInstance = new RegistrationIntentService();
    public static RegistrationIntentService getInstance(){
        return ourInstance;
    }
    private String key;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static String MY_TOKEN = "my_token";

/////////

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private SharedPreferences sharedPreferences;


    public RegistrationIntentService() {
        super(TAG);
        pref = MyApplication.getContet().getSharedPreferences("my_pref", Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]

            /**키를 여기에서 받는다.*/
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);
            key = token;
            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(token);

            // Subscribe to topic channels
            subscribeTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            SettingManager.getInstance().setBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true);
            SettingManager.getInstance().setString(QuickstartPreferences.TOKEN,key);
//            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
//            editor.putString(MY_TOKEN,key).commit();

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);

            SettingManager.getInstance().setBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);


        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.

        //여기에다가 서버로 보내는거 씀
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

    public String getToken() {
        return pref.getString(MY_TOKEN,"");
    }
}

//
///**
// * Created by kangjungu1 on 2016. 5. 25..
// */
//
//import android.annotation.SuppressLint;
//import android.app.IntentService;
//import android.content.Intent;
//import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//import com.google.android.gms.iid.InstanceID;
//import com.jhjg.skhu_drive.R;
//
//import java.io.IOException;
//
///**
// * Created by saltfactory on 6/8/15.
// */
//public class RegistrationIntentService extends IntentService {
//
//    private static final String TAG = "RegistrationIntentService";
//
//    public RegistrationIntentService() {
//        super(TAG);
//    }
//
//    /**
//     * GCM을 위한 Instance ID의 토큰을 생성하여 가져온다.
//     * @param intent
//     */
//    @SuppressLint("LongLogTag")
//    @Override
//    protected void onHandleIntent(Intent intent) {
//
//        // GCM Instance ID의 토큰을 가져오는 작업이 시작되면 LocalBoardcast로 GENERATING 액션을 알려 ProgressBar가 동작하도록 한다.
//        LocalBroadcastManager.getInstance(this)
//                .sendBroadcast(new Intent(QuickstartPreferences.REGISTRATION_GENERATING));
//
//        // GCM을 위한 Instance ID를 가져온다.
//        /**키를 여기에서 받는다.*/
//        InstanceID instanceID = InstanceID.getInstance(this);
//        String token = null;
//        try {
//            synchronized (TAG) {
//                // GCM 앱을 등록하고 획득한 설정파일인 google-services.json을 기반으로 SenderID를 자동으로 가져온다.
//                String default_senderId = getString(R.string.gcm_defaultSenderId);
//                // GCM 기본 scope는 "GCM"이다.
//                String scope = GoogleCloudMessaging.INSTANCE_ID_SCOPE;
//                // Instance ID에 해당하는 토큰을 생성하여 가져온다.
//                token = instanceID.getToken(default_senderId, scope, null);
//
//                Log.i(TAG, "GCM Registration Token: " + token);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // GCM Instance ID에 해당하는 토큰을 획득하면 LocalBoardcast에 COMPLETE 액션을 알린다.
//        // 이때 토큰을 함께 넘겨주어서 UI에 토큰 정보를 활용할 수 있도록 했다.
//        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("token", token);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
//    }
//}