package com.jhjg.skhu_drive.GCM;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.jhjg.skhu_drive.R;

public class GCMDialogActivity extends Activity {

    public static final String TAG = "GCMDialogActivity";
    private String nMessage, nTitle;
    private android.widget.TextView gcmtitle;
    private android.widget.TextView gcmmessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Bundle bun = getIntent().getExtras();
        nTitle = bun.getString("title");
        nMessage = bun.getString("message");

        Log.e(TAG,"GCMDialogActivity");
        setContentView(R.layout.activity_gcmdialog);
        this.gcmmessage = (TextView) findViewById(R.id.gcm_message);
        this.gcmtitle = (TextView) findViewById(R.id.gcm_title);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        gcmtitle.setText(nTitle);
        gcmmessage.setText(nMessage);


    }

}
