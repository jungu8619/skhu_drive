package com.jhjg.skhu_drive.GCM;

/**
 * Created by kangjungu1 on 2016. 5. 25..
 */
public class QuickstartPreferences {

    public static final String REGISTRATION_READY = "registrationReady";
    public static final String REGISTRATION_GENERATING = "registrationGenerating";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String TOKEN = "token";
}
